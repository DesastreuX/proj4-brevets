*Author: Perat Damrongsiri
*Email: peratd@uoregon.edu

Calculation Rule

*Distance	|	Min speed	|	Max speed

*0-200		|		15		|		34

*200-400	|		15		|		32

*400-600	|		15		|		30

*600-1000	|		11.428	|		28

*>1000		|		13.333	|		26

*Using this table to calculate the time it take to reach the check point for example:

*500km minimum time will take 200/34 + 200/32 + 100/30